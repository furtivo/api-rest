<?php
use App\RoutesLoader;

define("ROOT_PATH", __DIR__ . "/..");

$app['monolog.logfile'] = ROOT_PATH . "/var/logs/app.log";

$app['api.version'] = "/v1";
$app['api.endpoint'] = "/api";

$app['db.config']= array(
	"db.options" => array(
        "driver" => "pdo_sqlite",
        "path" => realpath(ROOT_PATH . "/var/app.db"),
    ),
);

// Load Routes
$routesLoader = new App\RoutesLoader($app);
$routesLoader->products();
$routesLoader->productsList();